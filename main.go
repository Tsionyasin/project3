package main

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/Tsionyasin/project3.git/controller"
	"gitlab.com/Tsionyasin/project3.git/service"
)

var (
	userService    service.UserService       = service.New()
	usercontroller controller.UserController = controller.New(userService)
)

func main() {

	route := gin.Default()

	route.GET("/users", func(ctx *gin.Context) {
		ctx.JSON(200, usercontroller.FindAll())
	})

	route.POST("/users", func(ctx *gin.Context) {
		ctx.JSON(200, usercontroller.Save(ctx))
	})

	route.Run(":8080")
}
