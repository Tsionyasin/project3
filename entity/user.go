package entity

import "time"

type User struct {
	Id              string    `json:"id"`
	Username        string    `json:"username" validate:"required,min=3,max=12"`
	Password        string    `json:"password" validate:"required"`
	Email           string    `json:"email" validate:"required,email"`
	Address         string    `json:"address" validate:omitempty"`
	Profile_picture string    `json:"profile_picture" validate:"required"`
	Phone_number    string    `json:"phone_number" validate:"required,validatePhoneNumber"`
	CreatedAt       time.Time `json:"created_at"`
	UpdatedAt       time.Time `json:"updated_at"`
	DeletedAt       time.Time `json:"deleted_at"`
}
